import { apiUrls } from '../../config';
import { hashPassword } from './password';


export function registerUser(user) {

    user.password = hashPassword(user.password);
    user.confirmPassword = hashPassword(user.confirmPassword);
  
    user = JSON.stringify(user);
  
    return fetch(apiUrls.register, {
      method: "POST",
      headers: {"Content-Type" : "application/json"},
      body: user
    });
  }