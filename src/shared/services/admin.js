import {apiUrls, authenticationToken} from "../../config";

export function getAllUsers() {
    return fetch(apiUrls.adminGetAllUsers, {
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${window.sessionStorage.getItem(authenticationToken)}`
        }
    });
}

export function searchUser(username) {
    return fetch(apiUrls.adminFindUsers, {
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${window.sessionStorage.getItem(authenticationToken)}`
        },
        body: JSON.stringify({searchParameter: username})
    })
}

export function adminBlockUser(user) {
    return fetch(apiUrls.adminBlacklistUser, {
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${window.sessionStorage.getItem(authenticationToken)}`
        },
        body: JSON.stringify([user])
    });
}

export function adminUnblockUser(user) {
    return fetch(apiUrls.adminWhitelistUser, {
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${window.sessionStorage.getItem(authenticationToken)}`
        },
        body: JSON.stringify([user])
    })
}