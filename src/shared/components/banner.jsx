import React from 'react';
import BannerImage from "../assets/crapappbanner.png";

const Banner = () => {
    return (
        <div className="row">
            <div className="col s6 offset-s3">
                <div className="card">
                    <img src={BannerImage} className="banner" alt="logo crap app" />
                </div>
            </div>
        </div>
    );
}

export default Banner;